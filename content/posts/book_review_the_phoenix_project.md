---
title: "Book Review: The Phoenix Project"
date: 2020-08-17T18:22:19+06:00
draft: false
author: "Bakytzhan Seitkazin"
tags:
- books
- review
---

![devops](/images/devops.png)

This is an amazing book, I recommend  it. But when I started the book, after reading the first 20 pages, I wanted to stop reading. Because it looked like another success story, there couldn’t be useful information for me.

But then I started to see the same problems I faced in my technical career as a main character. And more interesting, I did the same steps as the main hero intuitively.

This book describes Bert, who become a new VP of IT operation, and his main goal to run successfully Phoenix project, the project which could save a business.

And in this story, we see a classical company organization, where IT is just a part of an organization, and business makes some money using digital technology, but IT isn’t the main competence of the company. The IT department there looks like an outsourcing company, they serve a business wishlist.

One of the good ideas from the book is to track all types of work that IT operations do. There are 4 types of work: changes, deployments, unwanted. When they started to track work in progress tickets, they saw that they do a lot of unmanaged work. Without a clear justification of unmanaged work, they couldn’t properly measure time to finish a task or project.

They froze all types of work and started creating a clear development and deployment process using Kanban desks. Put together a new cross-functional SWAT team, created scripts to set up the same virtual environments for Dev, QA and Production servers. After that, the operation team permitted developers to build, package, and deploy their code to production servers without involving IT operation guys. Using DevOps culture they accelerated code deployments dramatically and pushed new features for Unicorn project within a day.

Yes, they created DevOps culture, which is: create fast flow from left to right, then to take fast feedback from right to left, and to learn from failure and success.

Also, this book describes bottlenecks in the process and resources. About task priority, doing the right tasks, and measure the right things.

The last idea in the book is that IT is the part of the business and everyone should develop this competency in yourself. It is a must-have ability today.