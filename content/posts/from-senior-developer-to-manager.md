---
title: "From Senior Developer to Manager"
date: 2020-07-15T22:59:59+06:00
draft: false
author: "Bakytzhan Seitkazin"
tags:
- developer
- manager
- agile
---

I have been working as a Senior Software Developer for 3 years, participated in creating digital products for a telecommunication company and one neo digital bank. As a senior developer, I wanted to show up my ability to write maintainable code, implement the better architecture, and all that stuff I learned before. But, when you create some product, the programming part takes less than 10% of the whole time, there are a lot of meetings, collaborations with other teams, and other discussions. 

And after 4 months, I started writing code, and I finished the project in 2 weeks, my productivity was high, I could work typing code for 8-10 hours per day making interruption for coffee. That’s all. Another meeting, another discussion, another demonstration for business. 

One day, head-hunter messaged me about some opportunity to lead some dev team, they develop and maintain food delivery service, and in the future want to become Delivery on Demand Service. Their tech stack based on python, Django, Django rest framework, celery, and so on. But, they have some point, if I manage their dev team I must not write code. 

On the one hand, I like to write code, build an architecture, create a scalable solution. 

On the other hand, I want to create a quality product. Useful product. And I thought that I know how to create a loveable product with fewer amount of meetings, and how to care dev team.

My first day as a software manager was stressful. I didn’t work with the new dev team, they don’t know me. I prepared my Wellcome speech. I had to gain their trust, and I precisely told them what I am doing here and what is my mission: “My main work here is to protect you! My job is to solve all your problems. I am a firewall between you and the business unit. And if you have some problems right now, tell me, and I will start from solving them” - said I. But they told me nothing of course, and continued working like before. The main idea in my head was, why do you think that you could manage dev team, just go home and write code, managing not for you. 

I truly believe that the manager shouldn’t tell them how to work. I trust in their professionalism.

So I started from the development process. Before the business asked to add some features, the dev team analyzed the purpose, gave some time prediction, and implemented it. They didn’t analyze why they implement that feature, didn’t measure time to market of the developed feature, there are no tests, just code writing.

I started from small. I created a presentation about software development methodologies, and we decided to go with Scrum within one-week cycles. But, when we started a sprint, there were about 1-2 stickers for each story, it looked like a bad assumption. After that, I wrote TD (technical debt) user story, and the dev team created more than 20 tasks! The dev team rushed by business forgot about real customers and their needs. 

How I said before, my main mission is to protect the dev team. I froze the development of business features for 3 months (summertime not a season for business) and focused on refactoring. First of all, we measured the cyclomatic complexity of the codebase, and it was more than 20 (nowadays we configured linter for 4). Passed courses on how to work with Docker virtualization and how to write tests (Django tests not suitable, pytest much more useful). But there was a big big problem. Python version 2 in 2019!

It took about one year to migrate from python2 to python3. The main problem was that used libraries in python2 doesn’t exist in python3 or changed API or maintainer deleted/archived code base. 

Another big problem was our Monolith, we couldn’t shift more features and the system became a Big Ball of Madness. Instead of microservices architecture, we decided to move on with Service Oriented Architecture (SOA), using Domain Driven Development (DDD) approach we divided our monolith application to 7 independent services. 

The last technical problem was for us Django templates based view layer. We migrated from templates to the Vue app backed by Nuxt and it works perfectly for us. We don’t store business logic in the front-end, and our codebase as simple as possible and covered with tests (nowadays about 90% test coverage).

Finally, the size of the dev team increased 4 times, we have well-organized cross-functional dev teams, we iterate fast and deploy code to production every day and we want to increase that value. If in a one-day business wants to double the dev team size we could to it easily, because the complexity of code is low, and the process of onboarding a new developer is fast. To run the whole our system the new developers need to run just one command - `docker-compose up`! It took one year to rebuild the system and create a scalable, robust, and transparent platform. 

Also, I created a grading system for developers, a personal improving roadmap considering hard and soft skills, and an assessment program to level-up in the grading system. The developers positively reacted for a new grading system, especially for the personally developed roadmap. And most developers shy to ask new grade by yourself, and they fill themself comfortable if there are some official process exist.
