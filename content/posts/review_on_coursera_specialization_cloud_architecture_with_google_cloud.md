---
title: "Review on Coursera Specialization: Cloud Architecture with Google Cloud"
date: 2020-11-18T16:42:26+06:00
draft: false
author: "Bakytzhan Seitkazin"
tags:
- courses
- coursera
- review
---

# Overview

I recently finished the `Coursera Specialization` program - `Cloud Architecture with Google Cloud` which contains seven courses:
* Google Cloud Platform Fundamentals: Core Infrastructure
* Essential Google Cloud Infrastructure: Foundation
* Essential Google Cloud Infrastructure: Core Services
* Elastic Google Cloud Infrastructure: Scaling and Automation
* Reliable Google Cloud Infrastructure: Design and Process
* Architecting with Google Kubernetes Engine: Foundations
* Preparing for the Google Cloud Professional Cloud Architect Exam

If you just want to familiarize with GCP and how to start work with it, I recommend the first course about `Core Infrastructure`. In this course they describe the architecture behind GCP, how to scale, networking, security and policy, types of compute engines, how they differ from each other and which one to choose. 

# Compute Engines

![compute_engine](/images/coursera_google_cloud/compute_engine.png)

GCP `Compute Engine` is classical `IaaS`, where you have full control of `compute`, `storage`, and `network`.

`App Engine` provides support for four programming languages (Java, JS, Ruby, Python) and all of these packages. If you need some specific version of language you could use managed App Engine.

In the `IaaS` model, you pay for what you `allocate`. In the `PaaS` model,  you pay for what you `use`.

You could use managed `Kubernete Engine` if you want to run microservices architecture. They added some functionality to classical `Kubernete` to make it handfull for elastically allocate and manage resources. 

`Cloud Functions` handful if you need to run just some functionality and you don’t care about infrastructure, you only need the result of that function. 

# Storage in the Cloud

Also they give a plethora of information about their `Data Storage Engines` like `Cloud Datastore`, `Cloud Storage`, `CloudSQL`, `Cloud Spanner`, `BigTable` and good practice of using them.

![storage](/images/coursera_google_cloud/storage.png)

`Cloud Storage` - or `Blob` storage, engine to store the files.

Consider using `Cloud SQL` or `Cloud Spanner` if you need full `SQL` support for an online transaction processing system. `Cloud Spanner` has ability to scale, `Cloud SQL` more functional as a DB Service for classical web application.

Consider using Cloud Datastore, if you need to store structured objects, or if you require support for transactions and SQL-like queries. This storage service provides terabytes of capacity with a maximum unit size of 1 MB per entity.

Consider using `Cloud Bigtable`, if you need to store a large amount of structured objects. `Cloud Bigtable` does not support `SQL` queries, nor does it support `multi-row transactions`. This storage service provides petabytes of capacity with a maximum unit size of 10 MB per cell and 100 MB per row.

# Containers in the Cloud

GCP provides support for containerization based on VM in the Compute Engine. This is achieved by providing an interface for `Docker images`, `Deployment Manager` and `Kubernetes` to orchestrate them.

# Summary

GCP provides additional services like `Deployment Manager`, `Image Register`, `Logs` and `metric`, `network balancer`, `CDN`, scale to multiple `Regions` and `Zones`, `firewall` rules and so on to make app development and operation comfortable in the Cloud and flexible pricing for computing service.

The main good feature of that course is the practice part based on `QwikLab` platform. They gave real GCP instances for temporary student accounts to implement some real cases using GCP solutions, starting from creating a simple `Virtual Machine`, to configuring `Kubernetes` engines and `NAT Load Balancer`.