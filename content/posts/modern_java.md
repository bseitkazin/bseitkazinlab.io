---
title: "Modern Java Notes"
date: 2020-11-26T19:08:58+06:00
draft: true
author: "Bakytzhan Seitkazin"
tags:
- java
---

# Fundamentals

## Java 8, 9, 10

For now, we’ll just say that the new `Streams` API behaves similarly to Java’s existing `Collections` API: both provide access to sequences of data items. But it’s useful for now to keep in mind that `Collections` is mostly about `storing` and `accessing` data, whereas `Streams` is mostly about describing `computations` on data. The key point here is that the `Streams` API allows and encourages the elements within a stream to be processed in `parallel`.

**Parallelism in Java and no shared mutable state**. People have always said parallelism in Java is difficult, and all this stuff about synchronized is error-prone. Where’s the magic bullet in Java 8?
There are two magic bullets. First, the library handles partitioning—breaking down a big stream into several smaller streams to be processed in parallel for you. Second, this parallelism almost for free from streams, works only if the methods passed to library methods like filter don’t interact.

Changing a Java `interface` meant changing every class that `implements` it. Java 8 and 9 have started to address this. First, Java 9 provides a `module` system that provide you with syntax to define modules containing collections of packages—and keep much better control over visibility and namespaces. Second, Java 8 added default methods to support `evolvable`interfaces.

## Passing code with behavior parameterization

`Behavior parameterization` is a software development pattern that lets you handle frequent requirement changes. In a nutshell, it means taking a block of code and making it available without executing it. This block of code can be called later by other parts of your programs, which means that you can defer the execution of that block of code. For instance, you could pass the block of code as an argument to another method that will execute it later. As a result, the method’s behavior is parameterized based on that block of code.

