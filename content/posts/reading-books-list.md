---
title: "Reading Books List"
date: 2020-07-16T11:20:37+06:00
draft: false
author: "Bakytzhan Seitkazin"
tags:
- books
---

This is a list of books which I want to read this year. After completing the reading a book I will post a review about a book with useful notes and main idea of the book. And my recomendation, to read it or not.

## Programming

* :closed_book:Java Performance: The Definitive Guide
* :closed_book:Python Tricks. The Book
* :closed_book:Javascript Design Patterns
* :closed_book:Types and programming languages
* :closed_book:Operating System Concepts
* :green_book:Effective Java. 3rd :star::star::star::star::star:
* :closed_book:Designing Data Intensive Applications
* :closed_book:Site Reliability Engineering: How Google Runs Prod System
* :closed_book:System Performance: Enterprise and the Cloud
* :closed_book:The art of software testing
* :closed_book:Distributed Algorithms
* :closed_book:The art of multiprocessor Programming
* :closed_book:SQL Performance Explained
* :closed_book:Introduction to Algorithms, Cormen
* :closed_book:Domain Driven Design, Eric Evans
* :closed_book:Java Concurrency in Practice
* :closed_book:Scala and Spark for Big Data Analytics
* :closed_book:Microservices: From Design to Deployment

## Project Management

* :green_book: [The Phoenix Project]({{< relref "book_review_the_phoenix_project.md" >}}) :star::star::star::star::star:

## General

* :closed_book:On Writing Well: The Classic Guide to Writing Nonfiction
* :closed_book:The Way of the Superior Man
* :closed_book:The Human Stain
* :closed_book:White teeth. Zadie Smith
* :closed_book:True History of the Kelly Gang
* :closed_book:2666
* :closed_book:Fortress of Solitude

## Lifestyle

* :closed_book:The 4-Hour Work Week
* :closed_book:Show your Work
* :closed_book:Anything you Want
* :closed_book:Matsushita leadership. Lessons from 20th Century's Most Remarkable Entrepreneur
* :closed_book:The corrections. Jonathan Franzen
* :closed_book:Eating the Big Fish: Now Challenger Brands Can Compete Against Brand Leaders
* :closed_book:How Brands Grow
* :closed_book:Decode marketing

<!-- 
 :closed_book: - listed
 :green_book:  - read
 :blue_book:   - reading
 :star::star::star::star::star: - 5 stars
-->